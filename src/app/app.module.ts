import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DataInputComponent } from './data-input/data-input.component';
import { DataServiceService } from './data-service.service';
import { ShowCurrentPositionComponent } from './show-current-position/show-current-position.component';
import { DegreeToDirectionPipe } from './degree-to-direction.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DataInputComponent,
    ShowCurrentPositionComponent,
    DegreeToDirectionPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [DataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
