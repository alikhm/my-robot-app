import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'degreeToDirection'
})
export class DegreeToDirectionPipe implements PipeTransform {

  transform(deg: number, args?: any): any {
    return this.mapDeg(deg);
  }

  mapDeg(deg) {
    switch (deg) {
      case 0:
        return 'north';
      case 180:
        return 'south';
      case 90:
        return 'east';
      case 270:
        return 'west';

    }

  }

}
