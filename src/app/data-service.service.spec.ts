import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { DataServiceService } from './data-service.service';

describe('DataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [DataServiceService]
    });
  });

  it('should be created', inject([DataServiceService], (service: DataServiceService) => {
    expect(service).toBeTruthy();
  }));

  it('should return 0 degre for north', inject([DataServiceService], (service: DataServiceService) => {
    expect(service.getFaceDeg('north')).toBe(0);
  }));

  it('should set current position to {x:0,y:0,f:180}', inject([DataServiceService], (service: DataServiceService) => {
    let pos = '0,0,south';
    service.setCurentPos(pos);
    expect(service.currentPos.x).toBe(0);
    expect(service.currentPos.y).toBe(0);
    expect(service.currentPos.f).toBe(180);
  }));

  it('should rotate 90 degrees', inject([DataServiceService], (service: DataServiceService) => {
    service.setCurentPos('0,0,south');
    service.setFace("left");
    expect(service.currentPos.f).toBe(90);
  }));
 
  it('should move to the south', inject([DataServiceService], (service: DataServiceService) => {
    service.setCurentPos('0,0,south');
    service.moveRobot();
    expect(service.currentPos.y).toBe(1);
  }));
 
});
