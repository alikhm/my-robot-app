import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import { ShowCurrentPositionComponent } from './show-current-position.component';
import { DegreeToDirectionPipe } from '../degree-to-direction.pipe';
import { DataServiceService } from '../data-service.service';

describe('ShowCurrentPositionComponent', () => {
  let component: ShowCurrentPositionComponent;
  let fixture: ComponentFixture<ShowCurrentPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      declarations: [ShowCurrentPositionComponent, DegreeToDirectionPipe],
      providers: [DataServiceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCurrentPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
