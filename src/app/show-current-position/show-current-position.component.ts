import { Component, Input } from '@angular/core';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-show-current-position',
  templateUrl: './show-current-position.component.html',
  styleUrls: ['./show-current-position.component.css']
})

export class ShowCurrentPositionComponent {
  _combinedData = null;
  constructor(private _dataService: DataServiceService) { }

  @Input() set combinedData(value) {
    this._combinedData = value;
  } get combinedData() {
    return this._combinedData;
  }
}