import { async, ComponentFixture, TestBed, inject, fakeAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { DataServiceService } from '../data-service.service';
import { DataInputComponent } from './data-input.component';

describe('DataInputComponent', () => {
  let component: DataInputComponent;
  let fixture: ComponentFixture<DataInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpModule],
      declarations: [DataInputComponent],
      providers: [DataServiceService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('Report must be updated when data changes', inject([DataServiceService], fakeAsync((service: DataServiceService) => {
    let combinedData;
    spyOn(service, 'getCombinedData');
    service.processData([['place 0,0,south']]);
    component.dataChanged(JSON.stringify({ 'data': [['place 0,0,north', 'report']] }));
    service.getCombinedData.subscribe(res => {
      expect(res[0].currentPos.f).toBe(0);

    });
  })));
});
