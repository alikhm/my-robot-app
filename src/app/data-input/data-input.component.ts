import { Component, Input } from '@angular/core';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-data-input',
  templateUrl: './data-input.component.html',
  styleUrls: ['./data-input.component.css']
})
export class DataInputComponent {

  private _data = null;
  errorMsg: string = null;

  @Input() set data(value) {
    this._data = JSON.stringify(value);
  } get data() {
    return this._data;
  }

  constructor(private _dataService: DataServiceService) { }

  dataChanged(data) {
    try {
      this._dataService.processData(JSON.parse(data).data);
      this.errorMsg = null;
    } catch (e) {
      this.errorMsg = "Data corrupted. Not JSON";
    }
  }
}
