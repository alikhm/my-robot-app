import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class DataServiceService {
  dataUrl: string = '../assets/data.json';
  currentPos = null;
  combinedData = [];
  allFaces: string[] = ['north', 'south', 'east', 'west']

  getData: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  getCurrPos: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  getCombinedData: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  constructor(private _http: Http) {
    this._http.get(this.dataUrl)
      .map(res => res.json())
      .subscribe(res => {
        this.getData.next(res);
        this.processData(res.data);
      });
  }

  processData(data) {
    //Reseting combined data
    this.combinedData = [];
    this.getCombinedData.next(this.combinedData);

    for (let i = 0; i < data.length; i++) {
      let dataItem = data[i];
      // Reseting currentPos for this iteration
      this.currentPos = null;
      for (let command of dataItem) {
        command = command.toLowerCase();

        if (command === 'right' || command === 'left') {
          this.setFace(command)
        } else if (command === 'move') {
          this.moveRobot();
        } else if (command.indexOf('place') !== -1) {
          this.setCurentPos(command.split(' ')[1]);
        } else if (command === 'report') {
          this.combinedData[i] = {
            data: dataItem,
            currentPos: this.currentPos ? Object.assign({}, this.currentPos) : null
          };
          this.getCombinedData.next(this.combinedData);
        }
      }
    }
  }

  setCurentPos(pos) {
    pos = pos.split(",");
    let x: number = parseInt(pos[0]);
    let y: number = parseInt(pos[1]);
    let f: string = pos[2];

    if (x >= 0 && x < 5 && y >= 0 && y < 5 && this.allFaces.indexOf(f) !== -1)
      this.currentPos = {
        x: x,
        y: y,
        f: this.getFaceDeg(f)
      }
  }

  getFaceDeg(face: string) {
    if (this.allFaces.indexOf(face) !== -1) {
      switch (face) {
        case 'north':
          return 0;
        case 'south':
          return 180;
        case 'east':
          return 90;
        case 'west':
          return 270;

      }
    };
  }

  setFace(dir: string) {
    if (this.currentPos) {
      switch (dir) {
        case 'right':
          this.currentPos.f += 90;
          break;
        case 'left':
          this.currentPos.f -= 90;
          break;
      }

      if (this.currentPos.f === -90) this.currentPos.f = 270;
      if (this.currentPos.f === 360) this.currentPos.f = 0;
    };

    this.getCurrPos.next(this.currentPos);
  }

  moveRobot() {
    if (this.currentPos) {
      switch (this.currentPos.f) {
        case 0:
          this.currentPos.y -= this.currentPos.y > 0 ? 1 : 0;
          break;
        case 90:
          this.currentPos.x += this.currentPos.x < 4 ? 1 : 0;
          break;
        case 180:
          this.currentPos.y += this.currentPos.y < 4 ? 1 : 0;
          break;
        case 270:
          this.currentPos.x -= this.currentPos.x > 0 ? 1 : 0;
          break;

      }
    }
    this.getCurrPos.next(this.currentPos);
  }
}
