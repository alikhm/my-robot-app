import { MyRobotAppPage } from './app.po';

describe('my-robot-app App', () => {
  let page: MyRobotAppPage;

  beforeEach(() => {
    page = new MyRobotAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
